import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login.vue';
import NewLogin from '@/components/NewLogin'
import Index from '@/components/Index'
import LoginRedirect from '@/components/LoginRedirect'
import JobInfo from '@/components/JobManager/JobInfo';
import AddMenu from '@/components/MenuManager/AddMenu';
import AddRoleMenu from '@/components/MenuManager/AddRoleMenu';
import LookPublish from '@/components/NoticeManager/LookPublish';
import PublishNotice from '@/components/NoticeManager/PublishNotice';
import EmployInfo from '@/components/EmployManager/EmployInfo';
import SelfInfo from '@/components/EmployManager/SelfInfo';
import MyResume from '@/components/EmployManager/MyResume';
// 请假
import LeaveApply from '@/components/ApprovalManager/LeaveApply';
// 离职
import QuitApply from '@/components/ApprovalManager/QuitApply';
// 报销
import SubmitApply from '@/components/ApprovalManager/SubmitApply';
import DeptInfo from '@/components/DeptManager/DeptInfo';
//我的申请
import MyApply from '@/components/MyApply/MyApply'; //请假
import QuitInfo from '@/components/MyApply/QuitInfo';//离职
import SubmitInfo from '@/components/MyApply/SubmitInfo';//报销
//审批管理
import ApplyDetail from '@/components/ApplyManager/ApplyDetail';
import AccountShare from '@/components/AccountManager/AccountShare';
import Test from '@/components/test/test.vue';
import AddRole from '@/components/RoleManager/AddRole';

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/loginOld',name: 'Login',component: Login},
    {path: '/',name: 'NewLogin',component: NewLogin},
    {path: '/index',name: 'Index',component: Index,
      redirect:'/loginRedirect',
      children:[
        {path: '/loginRedirect',name: 'LoginRedirect',component: LoginRedirect},
        {path: '/jobInfo',name: 'JobInfo',component: JobInfo},
        {path: '/lookNotice',name: 'LookPublish',component: LookPublish},
        {path: '/addMenu',name: 'AddMenu',component: AddMenu},
        {path: '/addRoleMenu',name: 'AddRoleMenu',component: AddRoleMenu},
        {path: '/publishNotice',name: 'PublishNotice',component: PublishNotice},
        {path: '/employInfo',name: 'EmployInfo',component: EmployInfo},
        {path: '/selfInfo',name: 'SelfInfo',component: SelfInfo},
        {path: '/deptInfo',name: 'DeptInfo',component: DeptInfo},
        {path: '/leaveApply',name: 'LeaveApply',component: LeaveApply},
        {path: '/quitApply',name: 'QuitApply',component: QuitApply},
        {path: '/submitApply',name: 'SubmitApply',component: SubmitApply},
        {path: '/myApply',name: 'MyApply',component: MyApply},
        {path: '/applyDetail',name: 'ApplyDetail',component: ApplyDetail},
        {path: '/quitInfo',name: 'QuitInfo',component: QuitInfo},
        {path: '/submitInfo',name: 'SubmitInfo',component: SubmitInfo},
        {path: '/myResume',name: 'MyResume',component: MyResume},
        {path: '/AccountShare',name: 'AccountShare',component: AccountShare},
        {path: '/test-son',name: 'Test',component: Test},
        {path: '/addRole',name: 'AddRole',component: AddRole},
      ]
    },
  ]
})
