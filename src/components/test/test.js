function menu(){
    let menus = [
        {menuId:202205100001,menuName:"公告管理",skipPath:"/index",menuSort:2,menuIcon:"el-icon-reading",menuCode:"",parentMenuId:"",
            childMenu:[
                {menuId:20220510001,menuName:"发布公告",skipPath:"/publishNotice",menuSort:1,menuIcon:"el-icon-chat-square",menuCode:"",parentMenuId:202205100001,childMenuId:[]},
                {menuId:20220510002,menuName:"查看公告",skipPath:"/lookNotice",menuSort:2,menuIcon:"el-icon-chat-dot-square",menuCode:"",parentMenuId:202205100001,childMenuId:[]},
            ]
        },
        {menuId:202205100002,menuName:"员工管理",skipPath:"/employManager",menuSort:1,menuIcon:"el-icon-user",menuCode:"",parentMenuId:"",
            childMenu:[
                {menuId:20220510003,menuName:"员工信息",skipPath:"/employInfo",menuSort:1,menuIcon:"el-icon-user-solid",menuCode:"",parentMenuId:202205100002,childMenuId:[]},
                {menuId:20220510004,menuName:"薪资信息",skipPath:"/moneyInfo",menuSort:2,menuIcon:"dollar",menuCode:"",parentMenuId:202205100002,childMenuId:[]},
            ]
        },
        {menuId:202205100003,menuName:"菜单信息",skipPath:"/menuInfo",menuSort:3,menuIcon:"el-icon-setting",menuCode:"",parentMenuId:"",
            childMenu:[
                {menuId:20220510005,menuName:"添加菜单",skipPath:"/addMenu",menuSort:1,menuIcon:"el-icon-circle-plus-outline",menuCode:"",parentMenuId:202205100003,childMenuId:[]}
            ]
        },
        {menuId:202205100004,menuName:"职位管理",skipPath:"",menuSort:4,menuIcon:"el-icon-c-scale-to-original",menuCode:"",parentMenuId:"",
            childMenu:[
                {menuId:20220510006,menuName:"职位信息",skipPath:"/jobInfo",menuSort:1,menuIcon:"el-icon-edit-outline",menuCode:"",parentMenuId:202205100004,childMenuId:[]}
            ]
        }
    ]
    return menus;
}

function jobData(){
    let data = [
        {id:1,jobName:"经理"},
        {id:2,jobName:"主管"},
        {id:3,jobName:"普通员工"},
        {id:4,jobName:"普通员工"},
        {id:5,jobName:"普通员工"},
        {id:6,jobName:"普通员工"},
        {id:7,jobName:"普通员工"},
        {id:8,jobName:"普通员工"},
        {id:8,jobName:"普通员工"},
    ]
    return data;
}

export {
    menu,
    jobData
}