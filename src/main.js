// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

/*引入资源请求插件*/
import axios from 'axios';
Vue.prototype.$http = axios;
// axios.defaults.baseURL = 'http://127.0.0.1:9001/oasys';
// axios.defaults.baseURL = 'http://42.192.152.162/';
axios.defaults.baseURL = 'http://zhangye-wu-aop.cloud:9001/oasys';
axios.defaults.withCredentials=true;

import ElementUi from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUi)

import Vuex from 'vuex';
Vue.use(Vuex);

// 引入字节图标库样式
import {install} from '@icon-park/vue/es/all';
// Install  默认前缀是 'icon', 例如: 对于`People`这个icon, 组件名字是`icon-people`
install(Vue);
import '@icon-park/vue/styles/index.css';

// 路由判断登录 根据路由配置文件的参数
router.beforeEach((to, from, next) => {
  let token=sessionStorage.getItem("token");
  if(to.path != "/"){
    if(token == null){
      Vue.prototype.$message.warning("用户未登录,请先登录!!!");
      next("/");
    }
    next();
  }
  next();
});

//请求拦截器
axios.interceptors.request.use(
  function(config){
    if(sessionStorage.getItem("token")){
      config.headers['Authorization'] = sessionStorage.getItem("token");
    }else{
      // 请求路径
      //Vue.prototype.$message.warning('未登录状态,请先登录!!!');
      console.log("请求地址:==>"+config.baseURL+config.url);
    }
    return config;
  },
  function(error){
    Vue.prototype.$message.error('请求超时,请重新登录!!!');
    return Promise.reject(error);
  }
);

// 响应拦截器
axios.interceptors.response.use(
  resp => {
    return resp.data;
  },
  (err) => {
    if(err.response.status === 401){
      alert("登录已过期，请重新登录");
      Vue.prototype.$message.error('请求超时,请重新登录!!!');
      let url = window.location.href;
      url = url.replace("http://", "").replace("https://", "");
      let baseUrl = url.split("/")[0];
      window.location.href = "http://" + baseUrl + "/";
    }
    try {
      return Promise.reject(err.response.data);
    } catch (e) {
      return Promise.reject(err);
    }
  }
);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
