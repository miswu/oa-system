#项目脚本文件
create table oa_sys.apply_type
(
    type_id    bigint      not null comment '申请类型id'
        primary key,
    apply_type varchar(45) null comment '申请类型'
) comment '审批类型表';
INSERT INTO oa_sys.apply_type (type_id, apply_type) VALUES (-1, '离职');
INSERT INTO oa_sys.apply_type (type_id, apply_type) VALUES (1, '事假');
INSERT INTO oa_sys.apply_type (type_id, apply_type) VALUES (2, '病假');
INSERT INTO oa_sys.apply_type (type_id, apply_type) VALUES (3, '婚假');
INSERT INTO oa_sys.apply_type (type_id, apply_type) VALUES (4, '产假');
INSERT INTO oa_sys.apply_type (type_id, apply_type) VALUES (5, '丧假');

create table oa_sys.oa_apply
(
    apply_id         bigint                                  not null comment '申请id'
        primary key,
    apply_title      varchar(500)  default ''                null comment '申请标题',
    type_id          bigint                                  null comment '申请类型id（-1离职 0入职 1事假 2病假 3产假）',
    apply_detail     varchar(4000) default ''                null comment '内容',
    apply_date       datetime      default CURRENT_TIMESTAMP null comment '申请时间',
    apply_user_id    bigint unsigned                         null comment '提交人userId',
    state_id         bigint                                  null comment '申请状态id（1待审批、2审批通过、3驳回）',
    approval_user_id bigint unsigned                         null comment '审批人',
    begin_date       datetime                                null comment '起始时间',
    end_date         datetime                                null comment '结束时间',
    reject_memo      varchar(4000) default ''                null comment '驳回原因'
)
    comment '审批表(请假/离职)';

create table oa_sys.oa_apply_state
(
    state_id    bigint      not null
        primary key,
    apply_state varchar(10) null comment '1待审批,2审批通过,3审批失败'
)
    comment '审批状态表';
INSERT INTO oa_sys.oa_apply_state (state_id, apply_state) VALUES (1, '待审批');
INSERT INTO oa_sys.oa_apply_state (state_id, apply_state) VALUES (2, '审批通过');
INSERT INTO oa_sys.oa_apply_state (state_id, apply_state) VALUES (3, '审批驳回');

create table oa_sys.oa_dept
(
    dept_id   bigint      not null comment '部门id 主键'
        primary key,
    dept_name varchar(45) not null comment '部门名称',
    dept_no   int         not null comment '部门编号'
)
    comment '部门表';
INSERT INTO oa_sys.oa_dept (dept_id, dept_name, dept_no) VALUES (1, '财务部', 1);
INSERT INTO oa_sys.oa_dept (dept_id, dept_name, dept_no) VALUES (2, '人事部', 2);
INSERT INTO oa_sys.oa_dept (dept_id, dept_name, dept_no) VALUES (3, '销售部', 3);
INSERT INTO oa_sys.oa_dept (dept_id, dept_name, dept_no) VALUES (4, '总务部', 4);

create table oa_sys.oa_job
(
    job_id   bigint     not null comment '职位id 主键'
        primary key,
    job_name varchar(8) null comment '职位 经理、主管、普通员工'
)
    comment '职位表';
INSERT INTO oa_sys.oa_job (job_id, job_name) VALUES (1, '经理');
INSERT INTO oa_sys.oa_job (job_id, job_name) VALUES (2, '主管');
INSERT INTO oa_sys.oa_job (job_id, job_name) VALUES (3, '普通员工');

create table oa_sys.oa_staff_ex
(
    sid           bigint                                not null comment '主键'
        primary key,
    user_id       bigint                                null comment '用户id',
    staff_name    varchar(45)                           not null comment '员工姓名',
    staff_age     varchar(4)                            null comment '员工年龄',
    staff_sex     varchar(5)                            null comment '员工性别',
    staff_sal     varchar(10)                           null comment '员工薪资',
    staff_tel     varchar(12)                           null comment '员工电话',
    shire_date    datetime    default CURRENT_TIMESTAMP null comment '入职时间',
    job_id        bigint      default -1                not null comment '职位外键 id',
    dept_id       bigint      default -1                not null comment '部门外键 id',
    staff_date    datetime    default CURRENT_TIMESTAMP null comment '出生日期',
    staff_address varchar(40) default ''                null comment '地址',
    state_id      bigint      default -1                not null comment ' 外键 状态：1在职/2离职/3停薪留职',
    snick_name    varchar(15)                           not null comment '登录用户名',
    staff_pwd     varchar(15)                           not null comment '登录密码'
)
    comment '员工扩展表';
INSERT INTO oa_sys.oa_staff_ex (sid, user_id, staff_name, staff_age, staff_sex, staff_sal, staff_tel, shire_date, job_id, dept_id, staff_date, staff_address, state_id, snick_name, staff_pwd) VALUES (1, 1001, 'manager', '23', '男', '******', '13225980516', '2022-05-12 21:16:50', 1, 1, '1998-01-14 21:17:31', '沈阳', 1, 'admin', 'admin');
INSERT INTO oa_sys.oa_staff_ex (sid, user_id, staff_name, staff_age, staff_sex, staff_sal, staff_tel, shire_date, job_id, dept_id, staff_date, staff_address, state_id, snick_name, staff_pwd) VALUES (2, 1003, 'manager', '25', '女', '*******', '13390906666', '2022-05-26 09:27:58', 2, 3, '2022-05-26 09:28:11', '本溪辽宁科技', 1, '文济鹏主管', 'admin');
INSERT INTO oa_sys.oa_staff_ex (sid, user_id, staff_name, staff_age, staff_sex, staff_sal, staff_tel, shire_date, job_id, dept_id, staff_date, staff_address, state_id, snick_name, staff_pwd) VALUES (3, 1002, 'user', '22', '女', '23232', '15041485001', '2022-05-25 22:17:54', 3, 2, '2022-05-25 22:18:09', '沈阳', 1, '文济鹏', 'admin');


create table oa_sys.oa_staff_state
(
    state_id    bigint      not null comment '状态id'
        primary key,
    staff_state varchar(45) null comment '员工状态: 在职,离职,停薪留职',
    state_no    int         not null comment '状态编号,不可重复'
)
    comment '人员状态表';
INSERT INTO oa_sys.oa_staff_state (state_id, staff_state, state_no) VALUES (1, '在职', 1);
INSERT INTO oa_sys.oa_staff_state (state_id, staff_state, state_no) VALUES (2, '离职', 2);
INSERT INTO oa_sys.oa_staff_state (state_id, staff_state, state_no) VALUES (3, '停薪留职', 3);

create table oa_sys.oa_submit_apply
(
    submit_id        bigint                             not null comment '报销id'
        primary key,
    submit_title     varchar(30)                        null comment '报销标题',
    type_id          bigint                             null comment '报销类型id 外键',
    submit_detail    varchar(100)                       null comment '报销内容',
    submit_img       varchar(300)                       null comment '发票/照片(地址)',
    submit_money     decimal  default 0                 null comment '报销金额',
    submit_date      datetime default CURRENT_TIMESTAMP null comment '申请日期',
    state_id         bigint                             null comment '审批状态id 外键',
    apply_user_id    bigint                             null comment '申请人id',
    approval_user_id bigint                             null comment '审批人id'
)
    comment '报销审批表';



    create table oa_sys.oa_submit_type
(
    type_id   bigint      not null comment 'id'
        primary key,
    type_name varchar(45) null comment '类型名称(出差/招待费/水电费/维修费/材料费)'
)
    comment '报销类型表';
INSERT INTO oa_sys.oa_submit_type (type_id, type_name) VALUES (1, '出差报销');
INSERT INTO oa_sys.oa_submit_type (type_id, type_name) VALUES (2, '招待费');
INSERT INTO oa_sys.oa_submit_type (type_id, type_name) VALUES (3, '水电费');
INSERT INTO oa_sys.oa_submit_type (type_id, type_name) VALUES (4, '维修费');
INSERT INTO oa_sys.oa_submit_type (type_id, type_name) VALUES (5, '材料费');


create table oa_sys.sys_menu
(
    menu_id        bigint unsigned auto_increment comment '菜单Id'
        primary key,
    menu_name      varchar(200) default '' not null comment '菜单名称',
    menu_code      varchar(200) default '' not null comment '菜单编码',
    parent_code    varchar(200) default '' not null comment '父菜单编码',
    menu_icon      varchar(500) default '' not null comment '菜单图标',
    menu_type      smallint     default 0  not null comment '菜单类别 0:权限 1:菜单',
    flag_invalid   smallint     default 0  not null comment '作废标识 0:作废 1:正常',
    skip_path      varchar(100)            null comment '跳转路径',
    menu_sort      int                     null comment '顺序',
    parent_menu_id bigint                  null comment '父菜单id',
    constraint sys_menu_menu_code_uindex
        unique (menu_code)
)
    comment '菜单表' auto_increment = 24;
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (1, '员工管理', 'employ_manager', '', 'el-icon-user', 1, 1, null, 1, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (2, '员工信息', 'employ_info', 'employ_manager', 'el-icon-user-solid', 1, 1, '/employInfo', 1, 1);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (3, '菜单管理', 'menu_manager', '', 'el-icon-setting', 1, 1, null, 3, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (4, '添加菜单', 'add_menu', 'menu_manager', 'el-icon-circle-plus-outline', 1, 1, '/addMenu', 1, 3);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (5, '职位管理', 'job_manager', '', 'el-icon-c-scale-to-original', 1, 1, null, 2, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (6, '职位信息', 'job_info', 'job_manager', 'el-icon-edit-outline', 1, 1, '/jobInfo', 1, 5);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (7, '公告管理', 'notice_manager', '', 'el-icon-reading', 1, 1, null, 4, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (8, '发布公告', 'publish_notice', 'notice_manager', 'el-icon-chat-square', 1, 1, '/publishNotice', 1, 7);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (9, '查看公告', 'look_notice', 'notice_manager', 'el-icon-chat-dot-square', 1, 1, '/lookNotice', 2, 7);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (10, '通用审批', 'generic_approval', '', 'el-icon-folder-opened
', 1, 1, null, 5, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (11, '请假申请', 'leave_apply', 'generic_approval', 'el-icon-folder-add
', 1, 1, '/leaveApply', 1, 10);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (12, '部门管理', 'dept_manager', '', 'el-icon-monitor', 1, 1, null, 6, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (13, '部门信息', 'dept_info', 'dept_manager', 'el-icon-document', 1, 1, '/deptInfo', 1, 12);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (14, '个人信息', 'self_info', 'employ_manager', 'el-icon-s-custom', 1, 1, '/selfInfo', 2, 1);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (15, '个人简历', 'my_resume', 'employ_manager', 'el-icon-tickets', 1, 1, '/myResume', 3, 1);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (16, '离职申请', 'quit_apply', 'generic_approval', 'el-icon-date', 1, 1, '/quitApply', 2, 10);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (17, '报销申请', 'submit_apply', 'generic_approval', 'el-icon-document-add', 1, 1, '/submitApply', 3, 10);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (18, '我的申请', 'my_apply', '', 'el-icon-loading', 1, 1, null, 7, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (19, '请假信息', 'apply_info', 'my_apply', 'el-icon-notebook-1', 1, 1, '/myApply', 1, 18);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (20, '审批管理', 'apply_manager', '', 'el-icon-bank-card', 1, 1, null, 8, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (21, '申请详情', 'apply_detail', 'apply_manager', 'el-icon-news', 1, 1, '/applyDetail', 1, 20);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (22, '报销信息', 'submit_info', 'my_apply', 'el-icon-star-off', 1, 1, '/submitInfo', 2, 18);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (23, '离职信息', 'quit_info', 'my_apply', 'el-icon-warning', 1, 1, '/quitInfo', 3, 18);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (24, '添加菜单(新)', 'add_role_menu_new', 'menu_manager', 'el-icon-s-flag', 1, 1, '/addRoleMenu', 2, 3);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (984969925846106112, '账号信息', 'account-info', '', 'el-icon-document-checked', 1, 1, '', 10, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (984970881576992768, '账号分配', 'account-share', 'account-info', 'el-icon-collection-tag', 1, 1, '/AccountShare', 0, 984969925846106112);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (985219549848666112, '角色管理', 'role-manager', '', 'el-icon-info', 1, 1, '', 11, null);
INSERT INTO oa_sys.sys_menu (menu_id, menu_name, menu_code, parent_code, menu_icon, menu_type, flag_invalid, skip_path, menu_sort, parent_menu_id) VALUES (985219750202179584, '添加角色', 'add-role', 'role-manager', 'el-icon-circle-plus', 1, 1, '/addRole', 0, 985219549848666112);


create table oa_sys.sys_notice
(
    nid         bigint                             not null comment '公告id'
        primary key,
    note_title  varchar(30)                        null comment '标题',
    note_detail varchar(100)                       null comment '内容',
    user_id     bigint                             not null comment '发布者id',
    note_date   datetime default CURRENT_TIMESTAMP null,
    note_state  int      default 0                 null comment '信息状态 0:正常 1:隐藏'
);



create table oa_sys.sys_role
(
    role_id      bigint unsigned auto_increment comment '角色Id'
        primary key,
    role_name    varchar(100) default '' not null comment '角色名称',
    flag_invalid smallint     default 0  not null comment '作废状态',
    role_no      int                     null comment '角色编号,不可重复',
    constraint sys_role_role_name_uindex
        unique (role_name)
)
    comment '系统角色表' auto_increment = 4;
INSERT INTO oa_sys.sys_role (role_id, role_name, flag_invalid, role_no) VALUES (1, '普通员工', 0, 1);
INSERT INTO oa_sys.sys_role (role_id, role_name, flag_invalid, role_no) VALUES (2, '主管', 0, 2);
INSERT INTO oa_sys.sys_role (role_id, role_name, flag_invalid, role_no) VALUES (3, '经理', 0, 3);


create table oa_sys.sys_role_menu
(
    role_menu_id bigint unsigned auto_increment comment '主键'
        primary key,
    role_id      bigint unsigned default '0' not null comment '角色Id',
    menu_id      bigint unsigned default '0' not null comment '菜单Id'
)
    comment '角色菜单表' auto_increment = 24;
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (1, 3, 1);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (2, 3, 2);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (3, 3, 3);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (4, 3, 4);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (5, 3, 5);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (6, 3, 6);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (7, 3, 7);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (8, 3, 8);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (9, 3, 9);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (10, 3, 10);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (11, 3, 11);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (12, 3, 12);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (13, 3, 13);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (14, 3, 14);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (15, 3, 15);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (16, 3, 16);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (17, 3, 17);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (18, 3, 18);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (19, 3, 19);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (20, 3, 20);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (21, 3, 21);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (22, 3, 22);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (55, 3, 24);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979397617798610944, 3, 23);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979796674165604352, 1, 10);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979796674698280960, 1, 11);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979796675205791744, 1, 16);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979796725340307456, 1, 17);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979797197019152384, 2, 18);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979797197518274560, 2, 19);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979797198009008128, 2, 22);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979797198671708160, 2, 23);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979799348705492992, 2, 20);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979799349175255040, 2, 21);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979851045414895616, 2, 7);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979851045851103232, 2, 5);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979851046325059584, 2, 1);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979851046673186816, 2, 10);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979851047054868480, 2, 12);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852001636515840, 2, 2);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852002047557632, 2, 14);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852003251322880, 2, 15);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852003976937472, 2, 6);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852004488642560, 2, 8);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852005142953984, 2, 9);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852005646270464, 2, 11);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852006040535040, 2, 16);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852006631931904, 2, 17);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852007185580032, 2, 13);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852700415950848, 1, 1);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852700764078080, 1, 2);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852701124788224, 1, 14);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852701695213568, 1, 15);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852702051729408, 1, 7);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852702429216768, 1, 9);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852703263883264, 1, 18);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852703649759232, 1, 19);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852704039829504, 1, 22);
INSERT INTO oa_sys.sys_role_menu (role_menu_id, role_id, menu_id) VALUES (979852704572506112, 1, 23);


create table oa_sys.sys_user
(
    user_id       bigint unsigned auto_increment comment '用户Id'
        primary key,
    user_account  varchar(100) default '' not null,
    user_name     varchar(100) default '' not null comment '用户名',
    password_hash varchar(500) default '' not null comment '密码哈希值',
    password_salt varchar(500) default '' not null comment '密码盐值',
    flag_invalid  smallint     default 0  not null comment '作废标识 0正常 1作废',
    constraint sys_user_user_account_uindex
        unique (user_account)
)
    comment '系统用户' auto_increment = 1002;
INSERT INTO oa_sys.sys_user (user_id, user_account, user_name, password_hash, password_salt, flag_invalid) VALUES (1001, 'admin', 'admin', '94b84e24552385c07376e1a2716903b7', '11111', 0);
INSERT INTO oa_sys.sys_user (user_id, user_account, user_name, password_hash, password_salt, flag_invalid) VALUES (1002, 'user', 'wenjipeng', '94b84e24552385c07376e1a2716903b7', '11111', 0);
INSERT INTO oa_sys.sys_user (user_id, user_account, user_name, password_hash, password_salt, flag_invalid) VALUES (1003, 'manager', '文济鹏经理', '94b84e24552385c07376e1a2716903b7', '11111', 0);

create table oa_sys.sys_user_role
(
    user_role_id bigint unsigned auto_increment comment '主键'
        primary key,
    user_id      bigint unsigned default '0' not null comment '用户Id',
    role_id      bigint unsigned default '0' not null comment '角色Id'
)comment '用户角色表'
    auto_increment = 2;
INSERT INTO oa_sys.sys_user_role (user_role_id, user_id, role_id) VALUES (1, 1001, 3);
INSERT INTO oa_sys.sys_user_role (user_role_id, user_id, role_id) VALUES (2, 1002, 1);
INSERT INTO oa_sys.sys_user_role (user_role_id, user_id, role_id) VALUES (3, 1003, 2);