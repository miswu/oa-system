# oa_system_vue

> oa system stu

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

演示地址:
http://zhangye-wu-aop.cloud/oasys/#/
账号密码: user / 123456
普通用户权限,菜单较少！！！
